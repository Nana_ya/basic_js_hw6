function createNewUser() {
	let newUser = {
		firstName: prompt("Input your first name, please!"),
		lastName: prompt("Input your last name, please!"),
        birthday: prompt("Input your date of birth, please!(for example, 07.09.2022)"),
		
		getPassword() {
			let password = this.firstName.substr(0, 1) + (this.lastName).toLowerCase() + this.birthday.substring(6, 10);
			return password;
		},
        getLogin() {
			let login = (this.firstName.substr(0, 1) + this.lastName).toLowerCase();
			return login;
		},
		getFirstName() {
			return this.firstName;
		},
		setFirstName(newFirstName) {
			Object.defineProperty(this, "firstName", { value: newFirstName });
		},
		getLastName() {
			return this.lastName;
		},
		setLastName(newLastName) {
			Object.defineProperty(this, "lastName", { value: newLastName });
		},
        getAge() {
						let age = (new Date() - new Date(this.birthday.substring(6, 10), this.birthday.substring(3, 5) - 1, this.birthday.substring(0, 2)))/ (1000 * 60 * 60 * 24 * 365.25);
			return Math.floor(age);
		},
        setAge(newBirthday) {
			Object.defineProperty(this, "birthday", { value: newBirthday });
		},
        
	};
    Object.defineProperty(newUser, "firstName", {
        configurable: true,
        writable: false,
    });
    Object.defineProperty(newUser, "lastName", {
        configurable: true,
        writable: false,
    });
    Object.defineProperty(newUser, "birthday", {
        configurable: true,
        writable: false,
    });
	return newUser;
}

function test(obj){
    console.log(obj.getFirstName() + " " + obj.getLastName());
    console.log(obj.getLogin());
    console.log(obj.getAge());
    console.log(obj.getPassword());
}

let user1 = createNewUser();
test(user1);
user1.setFirstName("Alisa");
user1.setLastName("Loe");
user1.setAge("07.09.1999");
test(user1);

